/**
 * Created by Clinton on 5/31/2017.
 */
$( document ).ready(function() {

    var fileOutput = null;
    var fileName = null;
    var fileSerial = null;
    var fileCreated = null;
    var downloadURL = null;
    var fileSize = null;

    $.ajax({
        url: '/api/files',
        method: 'get',
        dataType: 'json'
    }).fail(function (jqXHR, status) {

    }).done(function (data) {

        $.each(data, function (key, value) {
            $.each(value, function (key, value) {
                console.log(value.Name);
                fileName = value.Name;
                fileCreated = moment(value.Created).format('M/D/YYYY h:mm:ss a');
                fileSerial = value.Serial;
                fileSize = formatBytes(value.Size, 1);
                downloadURL = window.location.protocol  + '//' + window.location.host + '/download/' + fileSerial;
                fileOutput += '<tr class="file-row" data-name="'+ fileName +'" data-trid="' + fileSerial + '"><td>' + fileName + '</td><td><a href="'+downloadURL+'">File Link</a></td><td>' + fileSize +'</td><td>' + fileCreated + '</td><td><i class="fa fa-trash-o fa-2x fa-color-danger clickable" style="padding-left:10px;" data-id="'+ fileSerial +'" data-action="deleteFile"></i></td></tr>'
            })
        });
        $('#files-table-body').append(fileOutput);
    });


    $(document).on('click',  '[data-action="deleteFile"]', function(event) {
        var button = $(this);
        var actionFile = button.data('id');
        var fileName = $('[data-trid=' + actionFile + ']').attr('data-name');

        swal.queue([{
            title: 'Delete ' + fileName + '?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#c6140c',
            cancelButtonColor: '#b7b7b7',
            confirmButtonText: 'Delete File',
            reverseButtons: true,
            showLoaderOnConfirm: true,
            showCloseButton: true,
            preConfirm: function () {
                return new Promise(function (event) {
                    $.ajax({
                        url: "/api/delete/" + actionFile,
                        //dataType: "json",
                        type: "post",
                        headers: {
                            "X-CSRF-Token": document.getElementsByTagName("meta")["gorilla.csrf.Token"].getAttribute("content")
                        }
                    }).error(function (jqXHR, status) {

                    }).done(function (data) {
                        $('[data-trid=' + actionFile + ']').remove();
                        swal({
                            type: 'success',
                            title: 'File Deleted!',
                            timer: 1200
                        });
                    })
                })
            }
        }]);
    });

});


// Source Credit http://stackoverflow.com/a/18650828
function formatBytes(bytes,decimals) {
    if(bytes == 0) return '0 Byte';
    var k = 1024; // or 1024 for binary
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}