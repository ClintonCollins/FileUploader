/**
 * Created by Clinton on 5/30/2017.
 */
$( document ).ready(function() {

    $('#upload-progress').hide();
    var uploadArea = new Dropzone("#uploadbox", {
        url: "/upload",
        maxFilesize: 8192,
        clickable: true,
        previewTemplate: document.getElementById('upload-progress').innerHTML,
        previewsContainer: false,
        headers: {
        "X-CSRF-Token": document.getElementsByTagName("meta")["gorilla.csrf.Token"].getAttribute("content")
        }
    });
    Dropzone.autoDiscover = false;
    var uploadStart = new Date();
    var secondsElapsed = new Date();
    var fileSize = 0;

    // Update the total progress bar
    uploadArea.on("totaluploadprogress", function(progress) {
        //console.log(progress);
    });

    uploadArea.on("sending", function(file) {
        $('#upload-progress').show();
        fileSize = file.size;
        uploadStart = new Date()
    });

    uploadArea.on("uploadprogress", function(file, progress, bytesSent) {
        secondsElapsed = ( new Date() - uploadStart) / 1000;
        var currentBytesSent = (progress * fileSize) / 100;
        var bytesPerSecond = currentBytesSent / secondsElapsed;
        $('#percentage').html(Math.round(progress) + "% -- " + formatBits(bytesPerSecond, 1) + "/s");
        $('.progress-bar').css('width', progress + "%");
        console.log("Elapsed time: " + secondsElapsed + " seconds // " + formatBits(bytesPerSecond, 1) + "/s");
    });

    uploadArea.on("success", function (file, response) {
        console.log(response);
        var fileSerial = response;
        $('.progress-bar').css('width', "0");
        $('#upload-progress').hide();
        swal({
            title: 'Success!',
            //text: 'Here\'s your link to download the file',
            type: 'success',
            //input: 'text',
            inputClass: 'downloadLink',
            allowOutsideClick: false,
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: 'Stay Here',
            cancelButtonColor: '#3085d6',
            cancelButtonText: 'View Files',
            html:
            '<div id="swal2-content" class="swal2-content" style="display: block;">Here\'s the link to download the file</div><br>' +
            '<div class="input-group">' +
            '<input id="linkvalue" type="text" class="form-control" value="'+ window.location.protocol  + '//' + window.location.host + '/download/' + fileSerial +'" aria-describedby="basic-addon1">' +
            '<i class="input-group-btn">' +
            '<button class="btn btn-secondary link clickable" data-clipboard-target="#linkvalue" type="button">' +
            '<i class="fa fa-clipboard"></i></button>' +
            '</span>' +
            '</div>'
        }).then(function () {

        }, function (dismiss) {
            console.log("Window closed");
            window.location.href = '/files';
        });
        //$('.swal2-buttonswrapper').append('<button class="swal2-confirm swal2-styled" type="button" style="background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);""><a href="/files">View Files</a></button>');
        //$('.downloadLink').remove();
        new Clipboard('.link');
    });

    // Hide the total progress bar when nothing's uploading anymore
    uploadArea.on("queuecomplete", function(progress) {
        //$('.progress-bar').css('display', "none");
    });
});

function formatBits(bytes,decimals) {
    if(bytes == 0) return '0 Byte';
    var k = 1024; // or 1024 for binary
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat(((bytes * 8) / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}