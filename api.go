package main

import (
	"encoding/json"
	"fmt"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
	"github.com/go-chi/chi"
)

/// This file is dedicated to functions that return information not seen as a direct view to consumers. Things like the login/logout function exist here. An actual API may exist here at some point. \\\

func installUploader(w http.ResponseWriter, r *http.Request) {
	dbuser := User{}
	db.Last(&dbuser, &dbuser)
	if dbuser.ID == 0 {
		username := r.FormValue("username")
		password := r.FormValue("password")
		email := r.FormValue("email")
		if username == "" || password == "" || email == "" {
			fmt.Fprintf(w, "You must fill out all fields completely")
			return
		}
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if err != nil {
			log.Fatal(err)
		}
		newuser := User{Username: username, Admin: true, Password: hashedPassword, Email: email}
		db.Create(&newuser)
		fmt.Fprintf(w, "Admin User Created")
	} else {
		http.Error(w, http.StatusText(404), 404)
	}
}

func createUser(w http.ResponseWriter, r *http.Request) {
	dbuser := getRequestUser(r)
	if dbuser.Admin {
		username := r.FormValue("username")
		password := r.FormValue("password")
		email := r.FormValue("email")
		if username == "" || password == "" || email == "" {
			fmt.Fprintf(w, "You must fill out all fields completely")
			return
		}
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if err != nil {
			log.Fatal(err)
		}
		newuser := User{Username: username, Admin: false, Password: hashedPassword, Email: email}
		db.Create(&newuser)
		fmt.Fprintf(w, "User Created")
	} else {
		http.Error(w, http.StatusText(404), 404)
	}
}

func deleteFile(w http.ResponseWriter, r *http.Request) {
	fileSerial := chi.URLParam(r, "fileserial")
	dbuser := getRequestUser(r)
	if fileSerial == "" {
		http.Error(w, http.StatusText(404), 404)
	}
	dbfile := UserFile{}
	fileSerialUUID, err := uuid.FromString(fileSerial)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
	}
	db.Where(&UserFile{UUID: fileSerialUUID}).First(&dbfile)
	if dbfile.OwnerID == int(dbuser.ID) {
		err := os.Remove(fmt.Sprintf("%s/media/%s/%s-%s", Directory, dbuser.Username, dbfile.UUID, dbfile.Name))
		if err != nil {
			fmt.Println("Error deleting file")
		}
		db.Delete(&dbfile)
		fmt.Fprintf(w, "File deleted successfully")
	} else {
		fmt.Fprintf(w, "Unauthorized access")
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	//fmt.Println(Sessions)
	sessionid, exists := r.Cookie("sessionid")
	if exists == nil {
		delete(Sessions, sessionid.Value)
	}
	cookie := http.Cookie{
		Name:    "sessionid",
		Value:   "",
		Path:    "/",
		MaxAge:  0,
		Expires: time.Now().Add(-100 * time.Hour),
	}
	http.SetCookie(w, &cookie)
	//fmt.Println(Sessions)
	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func listFiles(w http.ResponseWriter, r *http.Request) {
	type Files struct {
		Files []struct {
			Name    string
			Serial  uuid.UUID
			Created time.Time
			Size    int
		}
	}
	userFiles := Files{}
	dbuser := getRequestUser(r)
	dbfiles := []UserFile{}
	//userFiles := []string{}
	db.Find(&dbfiles, UserFile{OwnerID: int(dbuser.ID)})
	for _, value := range dbfiles {
		info, err := os.Stat(fmt.Sprintf("%s/media/%s/%s-%s", Directory, dbuser.Username, value.UUID, value.Name))
		fileSize := 0
		if err != nil {
			fmt.Println("Unable to Stat file: " + value.Name + "So we're removing it.")
			db.Delete(value)
		} else {
			fileSize = int(info.Size())
			userFiles.Files = append(userFiles.Files, struct {
				Name    string
				Serial  uuid.UUID
				Created time.Time
				Size    int
			}{
				Name:    value.Name,
				Serial:  value.UUID,
				Created: value.CreatedAt,
				Size:    fileSize,
			})
		}
	}
	response, _ := json.Marshal(userFiles)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(response))
}

func uploader(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(32 << 20)
	file, handler, err := r.FormFile("file")
	if err != nil {
		fmt.Println(err)
		return
	}
	originalFilename := handler.Filename
	defer file.Close()
	//fmt.Fprintf(w, "%v", handler.Header)
	dbuser := getRequestUser(r)
	dir := Directory + "/media/" + dbuser.Username
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		dir = Directory + "/media/" + dbuser.Username
		os.MkdirAll(dir, 0600)
	}
	fileSerial := uuid.NewV4()
	handler.Filename = fmt.Sprintf("%s-%s", fileSerial, handler.Filename)
	f, err := os.OpenFile(Directory+"/media/"+dbuser.Username+"/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	io.Copy(f, file)
	info, err := f.Stat()
	if err != nil {
	}
	dbfile := UserFile{Name: originalFilename, UUID: fileSerial, OwnerID: int(dbuser.ID), Size: int(info.Size())}
	db.Create(&dbfile)
	fmt.Fprintf(w, fileSerial.String())
}

func login(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")
	//hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	//if err != nil {
	//	log.Fatal(err)
	//}
	var dbuser = User{}
	db.Where(&User{Username: strings.ToLower(username)}).First(&dbuser)
	if dbuser.ID == 0 {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else {
		err := bcrypt.CompareHashAndPassword([]byte(dbuser.Password), []byte(password))
		if err != nil {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		} else {
			sessionid := uuid.NewV4()
			Sessions[sessionid.String()] = &Session{SessionID: sessionid, UserID: dbuser.ID, Expires: time.Now().Add(30 * 24 * time.Hour)}
			expiration := time.Now().Add(30 * 24 * time.Hour)
			cookie := http.Cookie{
				Name:    "sessionid",
				Value:   sessionid.String(),
				Expires: expiration,
				Path:    "/",
			}
			http.SetCookie(w, &cookie)
			originpath, exists := r.Cookie("originpath")
			if exists == nil {
				cookie := http.Cookie{
					Name:    "originpath",
					Value:   "",
					Path:    "/",
					MaxAge:  0,
					Expires: time.Now().Add(-100 * time.Hour),
				}
				http.SetCookie(w, &cookie)
				http.Redirect(w, r, originpath.Value, http.StatusSeeOther)
			} else {
				http.Redirect(w, r, "/", http.StatusSeeOther)
			}
		}
	}
}
