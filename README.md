# FileUploader
A simple File Uploader that will spit out a direct link to view/download your file. This is my first Go web application designed for learning.

# Project Motivation
This project was created so friends and I could easily share files without having to use an outside service. It's designed to be super
fast and self hosted.

# Getting started
Currently the project uses a sqlite database for all users. 
1) First build the project with: `go build`
2) Visit `http://YOURIP:8000/install` to create an admin user.
3) Visit `http://YOURIP:8000/addusers` to create additional users.
4) Have fun with the project and feel free to modify main.go to edit settings.

# Future Goals
I'll keep working on this to learn more about Go. All of my history comes from Python/Django so it's a completely new world.
I'm excited to add more features and make things more secure and robust.
