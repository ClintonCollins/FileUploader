package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/satori/go.uuid"
	"github.com/gorilla/csrf"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"
	"os/signal"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"strings"
)

type UserTest struct {
	Username string
	Password []byte
}

type Config struct {
	Address string `json:"Address"`
	Port int `json:"Port"`
	CSRFSecure bool `json:"CSRFSecure"`
	CSRFHTTPOnly bool `json:"CSRFHttpOnly"`
	CSRFSecretKey string `json:"CSRFSecretKey"`
	DatabaseName string `json:"DatabaseName"`
}

type User struct {
	gorm.Model
	Username  string `gorm:"not null;unique"`
	FirstName string `gorm:"type:varchar(100)"`
	LastName  string `gorm:"type:varchar(100)"`
	Email     string `gorm:"type:varchar(100)"`
	Password  []byte `gorm:"type:varchar(455); not null"`
	Admin     bool   `gorm:"default:False"`
}

type Session struct {
	SessionID uuid.UUID
	UserID    uint
	Expires   time.Time
}

type UserFile struct {
	gorm.Model
	Name    string
	UUID    uuid.UUID
	Owner   User `gorm:"ForeignKey:OwnerID"`
	OwnerID int
	Size    int
}

var (
	templates      = template.Must(template.New("").ParseGlob("templates/*.html"))
	db             *gorm.DB
	Sessions       = make(map[string]*Session)
	Directory, _   = os.Getwd()
	ProtectedPaths = "static|login|create|logout|download|install|favicon.ico"
	Closer = make(chan os.Signal, 1)
	config = new(Config)
)

func GracefulExit() {
	<- Closer
	shutdown()
	os.Exit(1)
}

// Initializes the database and anything else that is needed on startup \\
func init() {
	ReadConfig()
	database, err := gorm.Open("sqlite3", config.DatabaseName)
	if err != nil {
		log.Fatal(err)
	}
	db = database
	db.AutoMigrate(&User{}, &UserFile{})
	LoadSessionsFromFile()
}

// Called right before a graceful exit \\
func shutdown() {
	SaveSessionsToFile()
}

// Start go routines that will constantly run with the application \\
func startupRoutines() {
	go GracefulExit()
	go PeriodicSessionSave()
}

func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}

func main() {
	listenAddress := fmt.Sprintf("%s:%d", config.Address, config.Port)
	signal.Notify(Closer, os.Interrupt)
	startupRoutines()

	router := chi.NewRouter()
	CSRF := csrf.Protect([]byte(config.CSRFSecretKey),
		csrf.Secure(config.CSRFSecure),
		csrf.HttpOnly(config.CSRFHTTPOnly),
		csrf.Path("/"),
		csrf.CookieName("csrf"),
	)

	// MIDDLEWARE //
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.RequestID)
	//router.Use(MyMiddleWare())
	router.Use(Authorization())
	router.Use(middleware.Timeout(60 * time.Second))

	// ROUTING GET //
	router.Get("/", index)
	router.Get("/login", loginPage)
	router.Get("/logout", logout)
	router.Get("/download/{fileserial}", downloader)
	router.Get("/files", viewFiles)
	router.Get("/install", installUploaderView)
	router.Get("/adduser", createUserView)
	router.Get("/favicon.ico", faviconHandler)

	// ROUTING POST //
	router.Post("/login", login)
	router.Post("/upload", uploader)
	router.Post("/install", installUploader)
	router.Post("/adduser", createUser)

	// ROUTING GET API //
	router.Get("/api/files", listFiles)

	// ROUTING POST API //
	router.Post("/api/delete/{fileserial}", deleteFile)

	// EXECUTION //
	workDir, _ := os.Getwd()
	filesDir := filepath.Join(workDir, "static")
	FileServer(router, "/static", http.Dir(filesDir))
	http.ListenAndServe(listenAddress, CSRF(router))
}
