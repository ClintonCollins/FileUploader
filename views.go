package main

import (
	"fmt"
	"github.com/satori/go.uuid"
	"net/http"
	"github.com/gorilla/csrf"
	"github.com/go-chi/chi"
)

/// This file is dedicated to functions that must return an HTML view either through a template or basic HTML file \\\


func index(w http.ResponseWriter, r *http.Request) {
	templateArgs := map[string]interface{}{"CSRF": csrf.Token(r)}
	templates.ExecuteTemplate(w, "index.html", templateArgs)
	//http.ServeFile(w, r, "templates/index.html")
}

func downloader(w http.ResponseWriter, r *http.Request) {
	fileSerial := chi.URLParam(r, "fileserial")
	//dbuser := getRequestUser(r)
	if fileSerial == "" {
		http.Error(w, http.StatusText(404), 404)
	}
	dbfile := UserFile{}
	fileSerialUUID, err := uuid.FromString(fileSerial)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
	}
	db.Where(&UserFile{UUID: fileSerialUUID}).First(&dbfile)
	w.Header().Set("Content-Disposition", "inline; filename=\""+dbfile.Name+"\"")
	fileowner := User{}
	db.First(&fileowner, dbfile.OwnerID)
	dir := fmt.Sprintf("%s/media/%s/%s-%s", Directory, fileowner.Username, dbfile.UUID, dbfile.Name)
	http.ServeFile(w, r, dir)
}

func loginPage(w http.ResponseWriter, r *http.Request) {
	if isLoggedIn(r) {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
	templateArgs := map[string]interface{}{csrf.TemplateTag: csrf.TemplateField(r)}
	templates.ExecuteTemplate(w, "login.html", templateArgs)
}

func viewFiles(w http.ResponseWriter, r *http.Request) {
	templateArgs := map[string]interface{}{"CSRF": csrf.Token(r)}
	templates.ExecuteTemplate(w, "files.html", templateArgs)
	//http.ServeFile(w, r, "templates/files.html")
}

func installUploaderView(w http.ResponseWriter, r *http.Request) {
	dbuser := User{}
	db.Last(&dbuser, &dbuser)
	fmt.Println(dbuser.ID)
	if dbuser.ID == 0 {
		http.ServeFile(w, r, "templates/install.html")
	} else {
		http.Error(w, http.StatusText(404), 404)
	}
}

func createUserView(w http.ResponseWriter, r *http.Request) {
	dbuser := getRequestUser(r)
	if dbuser.Admin {
		http.ServeFile(w, r, "templates/addusers.html")
	} else {
		http.Error(w, http.StatusText(404), 404)
	}
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, Directory+"/static/img/favicon.ico")
}
