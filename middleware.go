package main

import (
	"context"
	"net/http"
	"regexp"
	"strings"
)

/// All middleware built for the File Uploader will exist in this file. \\\

type Adapter func(http.Handler) http.Handler

// Checks for a session on every page except for non protected paths \\
func Authorization() Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			pathArray := strings.Split(r.URL.Path, "/")
			protectedPath := true
			if len(pathArray) >= 1 {
				protectedRegex := "(?i)" + ProtectedPaths
				if regexp.MustCompile(protectedRegex).MatchString(pathArray[1]) {
					protectedPath = false
				}
			}
			sessionid, exists := r.Cookie("sessionid")
			if exists != nil && protectedPath {
				OriginPathCookieSet(w, r)
				http.Redirect(w, r, "/login", http.StatusSeeOther)
			} else if !protectedPath {
				h.ServeHTTP(w, r)
			} else {
				_, ok := Sessions[sessionid.Value]
				if !ok {
					OriginPathCookieSet(w, r)
					http.Redirect(w, r, "/login", http.StatusSeeOther)
				} else {
					userid := Sessions[sessionid.Value]
					dbuser := User{}
					db.First(&dbuser, userid.UserID)
					ctx := context.WithValue(r.Context(), "UserID", dbuser.ID)
					h.ServeHTTP(w, r.WithContext(ctx))
				}
			}
		})
	}
}

// This is only set as an example to create new Middleware //
func MyExampleMiddleWare() Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			//fmt.Println(r.RemoteAddr)
			h.ServeHTTP(w, r)
		})
	}
}
