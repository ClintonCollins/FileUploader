package main

import (
	"net/http"
	"time"
	"os"
	"fmt"
	"encoding/gob"
	"io/ioutil"
	"encoding/json"
	"crypto/rand"
	"encoding/base64"
)

/// This file is dedicated to helper functions that return or set information \\\


// Gets the current user from the database that is making the request \\
func getRequestUser(r *http.Request) *User {
	sessionid, _ := r.Cookie("sessionid")
	userid := Sessions[sessionid.Value]
	dbuser := User{}
	db.First(&dbuser, userid.UserID)
	return &dbuser
}

// Sets a cookie to the path the user tried to go to before being prompted to login so they can go back automatically on login \\
func OriginPathCookieSet(w http.ResponseWriter, r *http.Request) {
	expiration := time.Now().Add(1 * time.Hour)
	cookie := http.Cookie{
		Name:    "originpath",
		Value:   r.URL.Path,
		Expires: expiration,
		Path:    "/",
	}
	http.SetCookie(w, &cookie)
}

// Checks if a user is logged in and returns a bool \\
func isLoggedIn(r *http.Request) bool {
	sessionid, exists := r.Cookie("sessionid")
	if exists == nil {
		_, ok := Sessions[sessionid.Value]
		if !ok {
			return false
		} else {
			return true
		}
	} else {
		return false
	}
}

// Every 5 minutes save the current sessions to file \\
func PeriodicSessionSave() {
	rate := time.Minute * 5
	throttle := time.Tick(rate)
	for {
		<- throttle
		SaveSessionsToFile()
	}
}

// Loads sessions from sessions file \\
func LoadSessionsFromFile() {
	f, err := os.Open(Directory + "/sessions")
	if err != nil {
		fmt.Println(err)
	}
	decoder := gob.NewDecoder(f)
	decoder.Decode(&Sessions)
	f.Close()
}

// Saves sessions to sessions file \\
func SaveSessionsToFile() {
	f, err := os.Create(Directory + "/sessions")
	if err != nil {
		fmt.Println(err)
	}
	encoder := gob.NewEncoder(f)
	encoder.Encode(&Sessions)
	f.Close()
}

//func SetCSRFTokenHeader(w http.ResponseWriter, r *http.Request) {
//	w.Header().Set("X-CSRF-Token", csrf.Token(r))
//}

func loadConfig(file string) error {
	raw, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	err = json.Unmarshal(raw, &config)
	if err != nil {
		return err
	}
	return nil
}

func writeConfig()  {
	f, err := os.Create(Directory + "/config.json")
	if err != nil {
		fmt.Println(err)
	}
	encoder := json.NewEncoder(f)
	encoder.SetIndent("", "	")
	encoder.Encode(&config)
	f.Close()
}

func ReadConfig() {
	err := loadConfig(Directory + "/config.json")
	if err != nil {
		randomKey := make([]byte, 32)
		_, err := rand.Read(randomKey)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("Change the config file to reflect your settings!")
		config.Address = ""
		config.Port = 8000
		config.CSRFHTTPOnly = true
		config.CSRFSecure = false
		config.CSRFSecretKey = base64.StdEncoding.EncodeToString(randomKey)
		config.DatabaseName = "data.db"
		writeConfig()
	}
}